# Aspiring Degree Holder

## Alona Joy Pegarit

👋 Aspiring Degree Holder — 💌 alonajoypegarit@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_pegarit_alona.jpg](images/bsis_2_pegarit_alona.jpg)

### Bio

**Good to know:** classic opm enjoyer

**Motto:** The possibility of all those possibilities being possible is just another possibility that can possibly happen.

**Languages:** C, C++, C#, Java, Javascript

**Other Technologies:** Google suite hehe

**Personality Type:** [Adventurer (ISFP-T)](https://www.16personalities.com/profiles/6ea758fa61085)

<!-- END -->

# Aspiring game app developer

## ALGEN REY UBANG

👋 Aspiring game app developer — 💌 algenreyubang@studentlaverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_ubang_algen_rey.jpg](images/bsis_2_ubang_algen_rey.jpg)

### Bio

**Languages:** Python, anaconda, Java, c#, html

**Other Technologies:** cellphone

**Personality Type:** [Entrepreneur (ESTP-T)](https://www.16personalities.com/estp-personality)

<!-- END -->

# Aspiring FrontEnd Developer

## Armie Jean Miranda

👋 Aspiring Imbutido  — 💌 armiejeanmiranda@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_miranda_armiejean.jpg](images/bsis_2_miranda_armiejean.jpg)

### Bio
just armie.

**Good to know:** I am an introvert person but I can be an extrovert depending on my minute mood #minutemaid oyeh. Making mistakes is a proof that you are trying. I prefer choco than milk.Selenophile. 

**Motto:** “Brain works better on an empty stomach.” -or wala lang talaga makain?

-Ghrelin

**Languages:** C#, Java

**Other Technologies:** 

**Personality Type:** [Adventurer (ISFP-T)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Andrey Garcia

👋 Aspiring Game Developer — 💌 andreygarcia@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_garcia_andrey.jpg](images/bsis_2_garcia_andrey.jpg)

### Bio

**Good to know:** I'm a fan of kpop, anime, manga, music, arts, nature, etc. My hobbies are voice acting, graphic designing, doing digital arts, dancing, and singing (not good at it).

**Motto:** Don't worry, everything gonna be alright.

**Languages:** Java, C#, HTML, GDScript, Python and CSS (soon), Tagalog, Kapampangan, English, Japanese, Korean, etc. (All basic in level).

**Other Technologies:** VSCode, Godot, Krita, PixelArt, Autodesk, Ibispaint, Capcut, Picsart, Adobe Lightroom (All basic in level). 

**Personality Type:** [Adventurer (ISFP-A)](https://www.16personalities.com/isfp-personality)

<!-- END -->

# 🔎 Still searching for suitable IT Career 👨‍💻

<hr>

## Angelo Delos Santos

👉😔👈 Basta IT — Apalit, Pampanga
<br>💻 [Facebook](https://facebook.com/saiyantist)
<br>💌 angelo.delossantos000@gmail.com / 💌jeromedelossantos@student.laverdad.edu.ph
<hr>

![alt bsis_2_delossantos_angelo.jpg](images/bsis_2_delossantos_angelo.jpg)
💗😢 Peter Paul Factor and 9M others...                         0 Comments                         143k Shares

<hr>

### Bio

**Good to know:** I am ultra efficient at times, then ultra lazy sometimes. I like troubleshooting stuff on my own —may it be technology or not— any problem I think I can solve, I will.

**Motto:** Experience is the best teacher, so keep moving forward even if it is hard.

**Programming Languages:** Java, C#, MySQL

**Other Technologies:** MS Office, Canva, Adobe Photoshop

**Personality Type:** [The Architect (INTJ-T)](https://www.16personalities.com/profiles/a8f0de72ebcf3)


<br>

**Sheesh**

🥴
<br>
👇👈
<!-- END -->

# Aspiring Multimedia Programmer and Data Management Specialist

## Aurora Zafra Bactol

👋 Aspiring Multimedia Programmer and Data Management Specialist — 💌 aurorazafrabactol@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_bactol_aurorazafra.jpg](images/bsis_2_bactol_aurorazafra.jpg)

### Bio

**Good to know:** I love art and music.

**Motto:** If you have hope, you have peace. - DSR

**Languages:** Java, C#

**Other Technologies:** Godot

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/c8cd53a6a6ebf)

<!-- END -->

# Aspiring Egg Roll

## Azhelle Casimiro

👋 Aspiring Egg Roll — 💌 azhellecasimiro@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_casimiro_azhelle.jpg](images/bsis_2_casimiro_azhelle.jpg)

### Bio

Good to know: I'm a fan of kpop boy group named seventeen and they are thirteen members. Before I really hate any kind of insects if I see one I want to kill it, I can't have a peace of mind thinking there's an insect inside the same room with me. Now I'm okay with it but I still hate it.

Motto: Be You

Languages: Java, C#

Other Technologies: None

Personality Type: [Mediator (INFP-T)](https://www.16personalities.com/profiles/93dce4243c46b)

<!-- END -->

# Aspiring Web Developer or Data Analyst

## Daniel Teves

👋 Web Developer or Data Analyst — 💌 danielteves@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_teves_daniel.png](images/bsis_2_teves_daniel.png)

### Bio

**Good to know:** Throughout my entire Highschool days which thought me how to give importance and acknowledging others rather than just yourself is something that gives me joy inside which I can't explain. That is why if I can be of help to someone I make sure to grab that opportunity to do so.

**Motto:** Respect is not given but earned.

**Languages:** C#, Java, MySQL

**Other Technologies:** Unity

**Personality Type:** [Consul (ESFJ-A)](https://www.16personalities.com/profiles/7570e242c3f9f)

<!-- END -->

# Aspiring Front End Developer

## Denise Louise Chavez

👋 Aspiring Front End Developer  — 💌 deniselouisechavez@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_chavez_denise.jpg](images/bsis_2_chavez_denise.jpg)

### Bio


**Good to know:** An introvert person.

**Motto:** It's ok to fall, just don't fall apart

**Languages:** html, java, css

**Other Technologies:** 

**Personality Type:** [Adventurer (ISPF-A)](https://www.16personalities.com/isfp-personality)

<!-- END -->

# Aspiring Application Developer

## Gene Marc B. Llagas

👋 Aspiring to become a Application Developer — 💌 genemarcllagas@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_llagas_genemarc.jpg](images/bsis_2_llagas_genemarc.jpg)

### Bio

Good to know: That i am trying to do better every single day i'm not being sadboy hehehe.

Motto: I can't really say to take things easy, let's just give our best shot.

Languages: Java and C#

Other Technologies: VSCODE.

Personality Type: [Assertive Entrepreneur (ESTP-A)](https://www.16personalities.com/profiles/e2cb0e4a6361e)
<!-- END -->

# Aspiring Software Engineer  or Fishball Vendor -->

## Harry Reyes

😭 Aspiring Software Engineer or Fishball Vendor — 📧 harryreyes@student.laverdad.edu.ph — Mexico, Pampanga

![alt bsis_2_reyes_harry.jpg](images/bsis_2_reyes_harry.jpg)

### Bio

**Good to know:** I really want to learn how to code and program. As of now, I'm crying inside trying to fix version vulnerabilities of HonKit.

**Motto:** Yamaha– I mean, "Pass, or not pass"

**Languages:** Java, C#

**Other Technologies:** Replit, Unity Hub, Microsoft Visual Studio Code

**Personality Type:** [Adventurer (ISFP-T)](https://www.16personalities.com/profiles/924a872b113ff)

<!-- END -->

# Aspiring Web Designer

## Jannah Dela Rosa

👋 Aspiring Web Designer — 💌 jannahdelarosa@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_dela_rosa_jannah.jpg](images/bsis_2_dela_rosa_jannah.jpg)

### Bio

**Good to know:** I like to learn new things that could help me be a better version of myself. 

**Motto:** There is always a miracle, so do not lose hope.

**Languages:** Java, C#

**Other Technologies:** N/A

**Personality Type:** [Turbulent Mediator (INFP-T)](https://www.16personalities.com/profiles/ed010fdf89037)

<!-- END -->

# Aspiring Game Developer

## Jericho Borromeo

👋 Aspiring Game Developer — 💌 jerichoborromeo@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_borromeo_jericho.jpg](images/bsis_2_borromeo_jericho.jpg)

### Bio

**Good to know:** I love drawing...

**Motto:** The true art is not someone who is inpired but one who inspires others -Salvador Dali

**Languages:** C#, GdScript, Java

**Other Technologies:** Clip Paint Studio, Adobe Animate, Blender, Unity, Unreal Engine, Autodesk Pro, Krita

**Personality Type:** [Advocate (INFJ-T)](hhttps://www.16personalities.com/infj-personality)

<!-- END -->

# Aspiring Full-stack Web Developer

## Jethro Cadang

👋 Aspiring Full-stack Web Developer — 💌 jethrocadang@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_cadang_jethro.jpg](images/bsis_2_cadang_jethro.jpg)
### Bio

Good to know: I'm a fast learner, a good listener. I prefer good communication over brilliant ideas.

Motto: Do your best today so tommorow you will not regret.

Languages: Html, Css

Other Technologies: (none for now)

Personality Type: [Turbulent Mediator (INFP-T)](https://www.16personalities.com/profiles/5737bf8a25d39)free-personality-test/5737bf8a25d39)

<!-- END -->

# Aspiring Web Designer

## John Erish Berboso

👋 Aspiring Web Designer — 💌 johnerishberboso@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_berboso_johnerish.jpg](images/bsis_2_berboso_johnerish.jpg)

### Bio

**Good to know:** I love listening to musics, play online games and playing basketball

**Motto:** I'd rather make mistakes than make nothing at all.

**Languages:** Java, Html, c#

**Other Technologies:** N/A

**Personality Type:** [Defender (ISFJ-A)](https://www.16personalities.com/profiles/9e51e3b49cd9a)

<!-- END -->

# Aspiring Back End Developer<!-- the one you want to be -->

## John Michael Umali

👋 Aspiring Back End Developer — 💌 johnmichaelumali@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_Umali_John_Michael.jpg](images/bsis_2_Umali_John_Michael.jpg)

### Bio

**Good to know:** I'm a fast Learner and I love working with others and I believe that no man is an island

**Motto:** Dreams don't work unless you do

**Languages:** Java, C#, HTML

**Other Technologies:** Microsoft office, Vs Code

**Personality Type:** [Campainer (ENFP-T)](https://www.16personalities.com/enfp-personality)

<!-- END -->

# Aspiring Programmer/Web Developer

## Joshua Allador

👋 Aspiring Programmer/Web Developer — 💌 joshuaallador@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_allador_joshua.jpg](images/bsis_2_allador_joshua.jpg)

### Bio

**Good to know:** Hello, I'm joshua I love sleeping and watching movie.

**Motto:** Slow progress, still a progress.

**Languages:** C++, C#, JAVA, HTML and CSS

**Other Technologies:** Microsoft Powerpoint, word.

**Personality Type:** Assertive Virtuoso (ISTP-A) https://www.16personalities.com/profiles/2f2fea3f6badd

<!-- END -->

# Aspiring Microfinanace Officer

## Jossamarie Advincula

👋 Aspiring Microfinance Officer — 💌 jossamarieadvincula@student.laverdad.edu.ph

![bsisbsis_2_advincula_jossa.jpg![](images/bsis_2_advincula_jossa.jpg)

### Bio

**Good to know:** I'am easy going. I struggle understandings technical things. I forgot something so easily.

**Motto:** Complain before Compile

**Languages:** java and C#

**Other Technologies:**

**Personality Type:** [Turbulent Defender (ISFJ-T)](https://www.16personalities.com/profile)

<!-- END -->

# Aspiring Data Analyst

## Julie Rose Oyong

👋 Aspiring Data Analyst — 💌 julieroseoyong@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_julie_rose_oyong.jpg)](images/bsis_2_julie_rose_oyong.jpg))

### Bio

Good to know: I love resonating with instrumental music.

Motto: Forgive then forget.

Languages: C#, Java, MySQL

Other Technologies: Unity, Godot, MAMP

Personality Type: [Turbulent Mediator (INFP-T)](https://www.16personalities.com/profiles/b7295aa06fccf)

<!-- END -->

# Aspiring Web Developer

## Kayla Acosta

👋 Aspiring Web Developer — 💌 kaylaacosta@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_acosta_kayla.jpg](images/bsis_2_acosta_kayla.jpg)

### Bio

**Good to know:** I like punk rock music.

**Motto:** Be kind.

**Languages:** Javascript, C#

**Other Technologies:** ...

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/b9b74c10c5632)

<br>

<!-- END -->

# Aspiring Web Developer

## Mark Renzo Tupaz

👋 Aspiring Web Developer — 💌 markrenzotupaz@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_tupaz_markrenzo.jpg](images/bsis_2_tupaz_markrenzo.jpg)

### Bio

Good to know: I like playing basketball

Motto: Kung hindi mo kaya sumuko kana

Languages: C# , Java 

Other Technologies: AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

Personality Type: [Turbulent Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/5c9062f44fa89)

<!-- END -->

# Aspiring Patatas

## Paula Soleil Jabinal

👋 Aspiring Patatas — 💌 paulasoleiljabinal@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_jabinal_paula_soleil.jpg](images/bsis_2_jabinal_paula_soleil.jpg)

### Bio

**Good to know:** I love drawing, eating, and sleeping.

**Motto:** "Never gonna give you up. Never gonna let you down. Never gonna run around and desert you."

**Languages:**  C#, Java, HTML 

**Other Technologies:** Godot, Krita, and ClipStudio

**Personality Type:** [Defender ISFJ-T](https://www.16personalities.com/profiles/5496135b7e226)

<!-- END -->

# Aspiring Cyber Security Expert

## Peter Factor

👋 Aspiring Cyber Security Expert — 💌 peterpaulfactor@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_factor_peter.jpg](images/bsis_2_factor_peter.jpg)

### Bio

Good to know: Fluent in only one language, which is "LOVE"

Motto: Experience is the best teacher 

Languages:  Javascript, MySQL, C#, HTML, CSS

Other Technologies: MS word, excell, powerpoint, publisher

Personality Type: [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

SHEESH

<!-- END -->

# Aspiring SOftware Engineer

## Princess Teves

👋 Aspiring Software Engineer — 💌 princesseloisateves@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_teves_princess.jpg](images/bsis_2_teves_princess.jpg)

### Bio

**Good to know:** I love playing volleyball

**Motto:** Never do tomorrow what you can do today

**Languages:**  Java, C#, MySQL

**Other Technologies:** N/A

**Personality Type:** [Defender (ISFJ-T)](https://www.16personalities.com/profiles/f55a7823d789)

<!-- END -->

# Aspiring Front-end Developer

##  Rieza Espejo

👋 Aspiring Front-end Developert — 💌 riezaespejo@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_espejo_rieza.jpg](images/bsis_2_espejo_rieza.jpg)

### Bio

Good to know: I enjoy drinking coffee and listening to music when it rains.

Motto: Don't give up, kaya wag kang sumuko

Languages: C#, Java, MySQL

Other Technologies: VS

Personality Type: [Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/257812cfe20d6)

<!-- END -->

# Aspiring IDK

## Royye Agustin

👋 Aspiring IDK — 💌 royyeagustin@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_agustin_royye.jpg](images/bsis_2_agustin_royye.jpg)

### Bio

**Good to know:** I seek for interesting things and love procrastinating.

**Motto:** Happy heart, healthy life

**Languages:** low level in (Java, c#, MySql, HTML)

**Other Technologies:** N/A

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/profiles/363bea7cfe6e0)

<!-- END -->

# Aspiring IT Instructor

## Steffanie Egloso

👋 Aspiring IT Instructor  — 💌 steffanieegloso@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_egloso_steffanie.jpg](images/bsis_2_egloso_steffanie.jpg)

### Bio


**Good to know:** I am an active listener and proactive learner. I am looking forward to awaken my creativity in this field. Doing things right is a time-saver.

**Motto:** Do not stop when your tired, stop when your done.

**Languages:** C#, Java, Html

**Other Technologies:** 

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/enfj-personality)

<!-- END -->